package com.vitasoft.tech.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vitasoft.tech.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	Optional<Customer> findByEmail(String email);
	
	Optional<Customer> findByMobile(String mobile);
	
	Optional<Customer> findByPanCard(String panCard);
	
	Optional<Customer> findByAadharCard(String aadharCard);

	
	boolean existsByEmail(String email);

	boolean existsByName(String name);

	boolean existsBypanCardId(String panCard);

	
	
 	

}
