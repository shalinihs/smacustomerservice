package com.vitasoft.tech.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
@Table(name="customer_tab")

public class Customer {
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cust_id_col")
	private Long id;
	
	@NotNull(message="Name cannot be null")
	@Size(min=3,max=20,message="Name must be 3-20 size only")
	@Column(name="cust_name_col")
	private String name;
	
	@NotNull(message="Email cannot be empty")
//	@Pattern(regexp="[A-Za-z1-9\\.\\-\\,]+\\@[a-z]+\\.\\[a-z]")
	@Email(message="Its not a valid Email Formate ")
	@Column(name="cust_email_col")
	private String email;
	
	
	@NotNull(message="Gender Cannot be null")
	@Pattern(regexp="Male|Female")
	@Column(name="cust_gender_col")
	private String gender;
	
	
	@NotBlank
	@Column(name="cust_imgpath_col")
	private String imagePath;
	
	
	@Pattern(regexp="^[1-9]*[1-9]{1}[0-9]{9}$")
	@Column(name="cust_mobile_col")
	private String mobile;
	
	@Column(name="cust_addr_col")
	private String address;
	
	@Column(name="cust_pancard_col")
	private String panCard;
	
	@Column(name="cust_aadharcard_col")
	private String aadharCard;
	

}
