package com.vitasoft.tech.restcontroller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vitasoft.tech.entity.Customer;
import com.vitasoft.tech.exception.CustomerNotFoundException;
import com.vitasoft.tech.service.ICustomerService;
import com.vitasoft.tech.validator.CustomerValidator;

@RestController

@RequestMapping("/customer")
public class CustomerRestController {

	@Autowired
	ICustomerService service;
	@Autowired
	private CustomerValidator validator;

//	1. Create Customer
	@PostMapping("/create")
	public ResponseEntity<?> createCustomer(
			@Valid @RequestBody Customer customer
			)
	{
		ResponseEntity<?> response = null;
		Map<String,String> errors = validator.validate(customer);
		if(errors.isEmpty()) {
			Long id = service.saveCustomer(customer);
			response = new ResponseEntity<String>(
					"Customer '"+id+"' Created!",
					HttpStatus.OK);
		} else {
			response = new ResponseEntity<Map<String,String>>(
					errors,HttpStatus.BAD_REQUEST);
		}
		
		return response;
	
	}

//	2. Fetch All Customer
	@GetMapping("/all")
	public ResponseEntity<List<Customer>> getAllCustomer() {

		ResponseEntity<List<Customer>> response = null;
		List<Customer> list = service.getAllCustomer();

		response = ResponseEntity.ok(list);
		return response;
	}

//	3. Fetch One Customer
	@GetMapping("/fetch/{id}")
	public ResponseEntity<Customer> getOneCustomer(@PathVariable Long id) {
		ResponseEntity<Customer> reponse = null;
		try {
			Customer customer = service.getOneCustomer(id);
			reponse = new ResponseEntity<Customer>(customer, HttpStatus.OK);// One customer

		} catch (CustomerNotFoundException cnfe) {
			cnfe.printStackTrace();
			throw cnfe;// Global Exception Handler

		}
		return reponse;
	}

//	4. Fetch one Customer by email
	@GetMapping("/fetch/{mail}")
	public ResponseEntity<Customer> getOneEmployByEmail(@PathVariable String email) {
		ResponseEntity<Customer> response = null;
		try {
			Customer cust = service.getOneCustomerByEmail(email);
			response = new ResponseEntity<Customer>(cust, HttpStatus.OK);

		} catch (CustomerNotFoundException cnfe) {
			cnfe.printStackTrace();
			throw cnfe;

		}
		return response;
	}

//	5. Fetch one Customer by Pan card
	@GetMapping("/fetch/{pancard}")
	public ResponseEntity<Customer> getOneCustomerByPanCard(@PathVariable String panCard) {
		ResponseEntity<Customer> response = null;
		try {
			Customer cust = service.getOneCustomerByPanCard(panCard);
			response = new ResponseEntity<Customer>(cust, HttpStatus.OK);
		} catch (CustomerNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		return response;
	}

//	6. Fetch one customer by Aadhar card
	@GetMapping("/fetch/{aadharcard}")
	public ResponseEntity<Customer> getOneCustomerByAadharCard(@PathVariable String aadharcard) {
		ResponseEntity<Customer> response = null;
		try {
			Customer cust = service.getOneCustomerByAadharCard(aadharcard);
			response = new ResponseEntity<Customer>(cust, HttpStatus.OK);
		} catch (CustomerNotFoundException cnfe) {
			cnfe.printStackTrace();
			throw cnfe;
		}

		return response;
	}

//	7. Fetch one Customer by Mobile
	@GetMapping("/fetch/{mobile}")
	public ResponseEntity<Customer> getOneCustomerByMobile(@PathVariable String mobile) {
		ResponseEntity<Customer> response = null;
		try {
			Customer cust = service.getOneCustomerByMobile(mobile);
			response = new ResponseEntity<Customer>(cust, HttpStatus.OK);
		} catch (CustomerNotFoundException cnfe) {
			cnfe.printStackTrace();
			throw cnfe;
		}
		return response;
	}

}
