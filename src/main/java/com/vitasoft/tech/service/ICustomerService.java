package com.vitasoft.tech.service;

import java.util.List;

import com.vitasoft.tech.entity.Customer;

public interface ICustomerService {
	
	Long saveCustomer(Customer customer);
	
	Customer getOneCustomer(Long id);
	//CustomQuery
	Customer getOneCustomerByEmail(String email);
	Customer getOneCustomerByPanCard(String panCard);
	Customer getOneCustomerByAadharCard(String aadharCard);
	Customer getOneCustomerByMobile(String mobile);
	
	List<Customer> getAllCustomer();
}
