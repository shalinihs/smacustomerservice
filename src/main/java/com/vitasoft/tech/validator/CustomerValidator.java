package com.vitasoft.tech.validator;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vitasoft.tech.entity.Customer;
import com.vitasoft.tech.repository.CustomerRepository;

@Component
public class CustomerValidator {
	
	@Autowired
	private CustomerRepository repo;

	public Map<String, String> validate(@Valid Customer customer) {
		Map<String, String> errors = new HashMap<>();
		if(repo.existsByEmail(customer.getEmail())) {
			errors.put("email", customer.getEmail()+" already exist");
		}
		if(repo.existsByName(customer.getName())) {
			errors.put("name", customer.getName()+" already exist");
		}
		if(repo.existsBypanCardId(customer.getPanCard())) {
			errors.put("panCardId", customer.getPanCard()+" already exist");
		}
		return errors;
	}

}