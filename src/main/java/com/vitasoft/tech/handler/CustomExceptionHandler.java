package com.vitasoft.tech.handler;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.vitasoft.tech.exception.CustomerNotFoundException;
import com.vitasoft.tech.payload.response.ErrorMessage;

@RestControllerAdvice
public class CustomExceptionHandler {
	
	
   @ExceptionHandler(CustomerNotFoundException.class)
	public ResponseEntity<ErrorMessage> handleCustomerNotFoundException(CustomerNotFoundException cnfe) {
		return ResponseEntity.internalServerError().body(
				new ErrorMessage(
						
						cnfe.getMessage()
				       
				      ));
	}
   
   /*
    * If any Validation Error occur those are given as
    * List<ObjectError>
    * 
    */
   @ResponseStatus(HttpStatus.BAD_REQUEST)
   @ExceptionHandler(MethodArgumentNotValidException.class)
   public Map<String,String> handleValidationException(MethodArgumentNotValidException ex){
	   Map<String,String> errors=new HashMap<>();
	   
	   ex.getBindingResult().getAllErrors().forEach((error)->{
		   String field=((FieldError)error).getField();
		   String errorMsg=error.getDefaultMessage();
		   errors.put(field, errorMsg);
		   
	   });
	   
	   
	   return errors;
   }
	
	
	
	
	
	

}
