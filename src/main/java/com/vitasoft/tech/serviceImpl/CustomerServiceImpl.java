package com.vitasoft.tech.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vitasoft.tech.entity.Customer;
import com.vitasoft.tech.exception.CustomerNotFoundException;
import com.vitasoft.tech.repository.CustomerRepository;
import com.vitasoft.tech.service.ICustomerService;

@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	CustomerRepository repo;

	@Override
	public Long saveCustomer(Customer customer) {

		return repo.save(customer).getId();
	}

	@Override
	public Customer getOneCustomer(Long id) {
		Optional<Customer> opt = repo.findById(id);
		if (!opt.isPresent()) {
			throw new CustomerNotFoundException("Customer '" + id + "' does  not found");
		} else {
			return opt.get();
		}

	}

	@Override
	public Customer getOneCustomerByEmail(String email) {
		Optional<Customer> opt = repo.findByEmail(email);
		return validateInput(opt, "Email", email);
	}

	@Override
	public Customer getOneCustomerByPanCard(String panCard) {
		Optional<Customer> opt = repo.findByPanCard(panCard);
		return validateInput(opt, "Pan Card", panCard);
	}

	@Override
	public Customer getOneCustomerByAadharCard(String aadharCard) {
		Optional<Customer> opt = repo.findByAadharCard(aadharCard);
		return validateInput(opt, "AadharCard", aadharCard);
	}

	@Override
	public Customer getOneCustomerByMobile(String mobile) {
		Optional<Customer> opt = repo.findByMobile(mobile);
		return validateInput(opt, "Mobile", mobile);

	}

	@Override
	public List<Customer> getAllCustomer() {

		return repo.findAll();
	}

	private Customer validateInput(Optional<Customer> opt, String field, String data) {

		/*
		 * if(!opt.isPresent()) { throw new
		 * CustomerNotFoundException("Customer "+field+": '"+data+"' does  not found");
		 * }else { return opt.get(); }
		 */

//		Java 1.8 concept
		return opt.orElseThrow(
				() -> new CustomerNotFoundException("Customer " + field + ": '" + data + "' does  not found"));

	}

}
